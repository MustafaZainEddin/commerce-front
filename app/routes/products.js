import Route from '@ember/routing/route';

export default Route.extend({
  queryParams: {
    page: {
      refreshModel: true
    },
    size: {
      refreshModel: true
    },
    department_id: {
      refreshModel: true
    },
    product_name: {
      refreshModel: true
    },
    promotion_code: {
      refreshModel: true
    }
  },
  model(params) {
    return Ember.RSVP.hash({
      products: this.store.query('product', { page: {
          number: params.page,
          size: params.size,
        },
        department_id: params.department_id,
        product_name: params.product_name,
        promotion_code: params.promotion_code
      }),
      departments: this.store.findAll('department')
    });
  },

  setupController(controller, models) {
    controller.set('products', models.products);
    controller.set('departments', models.departments);
  }

});
