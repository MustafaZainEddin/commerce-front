import DS from 'ember-data';
const { Model, attr } = DS;

export default Model.extend({
  name: attr('string'),
  price: attr('number'),
  material: attr('string'),
  color: attr('string'),
  department: attr(),
  promotion: attr(),
  final: attr('number')
});
