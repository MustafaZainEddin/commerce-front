import Controller from '@ember/controller';
import Ember from 'ember';
import { computed } from '@ember/object';

export default Controller.extend({
  products: [],
  init () {
    this.set('products', Ember.get(this, 'products'));
  },
  queryParams: ['page', 'size', 'department_id', 'product_name', 'promotion_code'],
  page: 1,
  size: 30,
  department_id: undefined,
  product_name: undefined,
  promotion_code: undefined,
  tmp_promotion_code: undefined,
  next_button_disabled: computed('products', function () {
    let products = this.get('products');
    let page = this.get('page');
    if (page == products.meta.pagination.last.number) {
      return true;
    } else {
      return false;
    }
  }),
  prev_button_disabled: computed('products', function () {
    let products = this.get('products');
    let page = this.get('page');
    if (page == products.meta.pagination.first.number) {
      return true;
    } else {
      return false;
    }
  }),
  metaData: computed('products', function () {
    console.log('adsa');
    return {}
  }),
  checkStatus: function() {
    let products = Ember.get(this, 'products');
    let page = this.get('page');
    this.set('prev_button_disabled', false);
    this.set('next_button_disabled', false);


    if (page == products.meta.pagination.first.number) {
      this.set('prev_button_disabled', true);
    }

    if (page == products.meta.pagination.last.number) {
      this.set('next_button_disabled', true);
    }
  },
  actions: {
    nextPage() {
      let page = this.get('page');
      let next_page = page + 1;
      this.set('page', next_page);
    },
    prevPage() {
      let page = this.get('page');
      let prev_page = page - 1;
      this.set('page', prev_page);
    },
    changeDep(value) {
      this.set('department_id', value);
      this.set('page', 1);
    },
    searchProduct(value) {
      this.set('product_name', value);
      this.set('page', 1);
    },
    setPromoCode(value) {
      this.set('tmp_promotion_code', value);
    },
    promoSearch() {
      let promotion_code = this.get('tmp_promotion_code');
      this.set('promotion_code', promotion_code);
      this.set('page', 1);
    }
  }
});
